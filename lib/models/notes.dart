import 'package:flutter/cupertino.dart';
import 'package:flutter/widgets.dart';

class Notes {
  final String id;
  final String title;
  final String ap1;
  final String ap2;
  final String ap3;
  final String media;

  const Notes({
    this.id,
    @required this.title,
    this.ap1,
    this.ap2,
    this.ap3,
    this.media,
  });
}
