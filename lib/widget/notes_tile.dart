import 'package:calculadora_de_notas/models/notes.dart';
import 'package:calculadora_de_notas/provider/notes_provider.dart';
import 'package:calculadora_de_notas/routes/app_routes.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class NotesTile extends StatefulWidget {
  final Notes notes;
  const NotesTile(this.notes);
  @override
  _NotesTileState createState() => _NotesTileState();
}

class _NotesTileState extends State<NotesTile> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          padding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
          child: SizedBox(
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0),
              ),
              elevation: 7.0,
              shadowColor: Theme.of(context).primaryColor,
              child: Column(
                children: <Widget>[
                  // Nome da matéria
                  ListTile(
                    title: Center(
                      child: Text(
                        widget.notes.title,
                        style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 17.0,
                          color: Theme.of(context).primaryColor,
                        ),
                      ),
                    ),
                    // Editar notas
                    leading: Container(
                      width: 48,
                      child: Row(
                        children: <Widget>[
                          IconButton(
                            splashColor: Colors.orange,
                            icon: Icon(
                              Icons.edit,
                              color: Colors.orange,
                            ),
                            onPressed: () {
                              Navigator.of(context).pushNamed(
                                  AppRoutes.NOTES_FORM,
                                  arguments: widget.notes);
                            },
                          )
                        ],
                      ),
                    ),
                    // Excluir as notas
                    trailing: Container(
                      width: 48,
                      child: Row(
                        children: <Widget>[
                          IconButton(
                            splashColor: Colors.red,
                            icon: Icon(
                              Icons.delete,
                              color: Colors.red,
                            ),
                            onPressed: () {
                              showDialog(
                                context: context,
                                builder: (ctx) => AlertDialog(
                                  elevation: 7.0,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10.0),
                                  ),
                                  title: Text(
                                    'Excluir nota',
                                    style: TextStyle(color: Colors.red),
                                  ),
                                  content: Text(
                                    'Tem certeza?',
                                    style: TextStyle(
                                      color: Theme.of(context).primaryColor,
                                    ),
                                  ),
                                  actions: <Widget>[
                                    FlatButton(
                                      splashColor: Theme.of(context).primaryColor,
                                      child: Text(
                                        'Não',
                                        style: TextStyle(
                                          color: Color(0xff005492),
                                        ),
                                      ),
                                      onPressed: () {
                                        Navigator.of(context).pop();
                                      },
                                    ),
                                    FlatButton(
                                      color: Colors.red,
                                      splashColor: Colors.red,
                                      child: Text('Sim'),
                                      onPressed: () {
                                        Provider.of<NotesProvider>(context,
                                                listen: false)
                                            .remove(widget.notes);
                                        Navigator.of(context).pop();
                                      },
                                    )
                                  ],
                                ),
                              );
                            },
                          ),
                        ],
                      ),
                    ),
                  ),

                  // Divisor
                  Divider(
                    color: Color(0xff005492),
                  ),

                  // Média final
                  ListTile(
                    title: Center(
                      child: Text(
                        widget.notes.media,
                        // _calculate(),
                        style: TextStyle(
                          fontSize: 30.0,
                          // color: Colors.green,
                        ),
                      ),
                    ),
                    subtitle: Center(
                      child: Text(
                        'Média final',
                        style: TextStyle(
                          color: Color(0xff005492),
                        ),
                      ),
                    ),
                  ),
                  Row(
                    children: <Widget>[
                      // AP1
                      Expanded(
                        child: Form(
                          child: Container(
                            child: ListTile(
                              title: Center(
                                child: Text(
                                  widget.notes.ap1,
                                  // _nota,
                                  style: TextStyle(
                                    fontSize: 30.0,
                                  ),
                                ),
                              ),
                              subtitle: Center(
                                child: Text(
                                  'AP1',
                                  style: TextStyle(
                                    color: Color(0xff005492),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),

                      // AP2
                      Expanded(
                        child: Container(
                          child: ListTile(
                            title: Center(
                              child: Text(
                                widget.notes.ap2,
                                style: TextStyle(
                                  fontSize: 30.0,
                                  // color: Colors.green,
                                ),
                              ),
                            ),
                            subtitle: Center(
                              child: Text(
                                'AP2',
                                style: TextStyle(
                                  color: Color(0xff005492),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),

                      // AP3
                      Expanded(
                        child: Container(
                          child: ListTile(
                            title: Center(
                              child: Text(
                                widget.notes.ap3,
                                style: TextStyle(
                                  fontSize: 30.0,
                                  // color: Colors.green,
                                ),
                              ),
                            ),
                            subtitle: Center(
                              child: Text(
                                'AP3',
                                style: TextStyle(
                                  color: Color(0xff005492),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
