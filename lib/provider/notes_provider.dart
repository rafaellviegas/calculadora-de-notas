import 'dart:math';

import 'package:calculadora_de_notas/models/notes.dart';
import 'package:flutter/cupertino.dart';

class NotesProvider with ChangeNotifier {
  final Map<String, Notes> _items = {};

  List<Notes> get all {
    return [..._items.values];
  }

  int get count {
    return _items.length;
  }

  Notes byIndex(int i) {
    return _items.values.elementAt(i);
  }

  void put(Notes notes) {
    if (notes == null) {
      return;
    }
// void _calculate(){
//   setState
// }

    // Alterar
    if (notes.id != null && _items.containsKey(notes.id)) {
      _items.update(
        notes.id,
        (_) => Notes(
          id: notes.id,
          title: notes.title,
          ap1: notes.ap1,
          ap2: notes.ap2,
          ap3: notes.ap3,
          media: notes.media,
        ),
      );
    } else {
      // Adicionar
      final id = Random().nextDouble().toString();
      _items.putIfAbsent(
        id,
        () => Notes(
          id: id,
          title: notes.title,
          ap1: notes.ap1,
          ap2: notes.ap2,
          ap3: notes.ap3,
          media: notes.media,
        ),
      );
    }
    notifyListeners();
  }

// Excluir
  void remove(Notes notes) {
    if (notes != null && notes.id != null) {
      _items.remove(notes.id);
      notifyListeners();
    }
  }
}
