import 'package:calculadora_de_notas/models/notes.dart';

//
const NOTES_DATA = {
  '1': const Notes(
    title: 'Nome da matéria aqui',
    ap1: '5',
    ap2: '5',
    ap3: '5',
    media: '5',
  ),
  '2': const Notes(
    title: 'POO',
    ap1: '5',
    ap2: '5',
    ap3: '5',
    media: '5',
  ),
};
