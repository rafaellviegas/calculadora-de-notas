import 'package:calculadora_de_notas/models/notes.dart';
import 'package:calculadora_de_notas/provider/notes_provider.dart';
import 'package:calculadora_de_notas/routes/app_routes.dart';
import 'package:calculadora_de_notas/widget/notes_tile.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class NotesList extends StatelessWidget {
  final List<Notes> notes;

  NotesList(this.notes);
  @override
  Widget build(BuildContext context) {
    final NotesProvider notesProvider = Provider.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text('Minhas notas'),
        centerTitle: true,
        actions: [
          IconButton(
            icon: Icon(Icons.add),
            onPressed: () {
              Navigator.of(context).pushNamed(AppRoutes.NOTES_FORM);
            },
          ),
        ],
      ),
      // Se a tela estiver vazia, ela irá retornar um texto com uma imagem, caso contrário irá retornar as notas adicionadas.
      body:
          // notes.isEmpty
          //     ? LayoutBuilder(
          //         builder: (ctx, constraints) {
          //           return Column(
          //             children: [
          //               SizedBox(height: 20.0),
          //               Text(
          //                 'Aguardando as suas notas! :)',
          //                 style: TextStyle(color: Theme.of(context).primaryColor),
          //               ),
          //               SizedBox(height: 20.0),
          //               Container(
          //                 height: constraints.maxHeight * 0.5,
          //                 child: Image.asset(
          //                   'assets/images/waiting.png',
          //                   fit: BoxFit.cover,
          //                   color: Theme.of(context).primaryColor,
          //                 ),
          //               ),
          //             ],
          //           );
          //         },
          //       )
          //     :
          ListView.builder(
        itemCount: notesProvider.count,
        itemBuilder: (ctx, i) => NotesTile(
          notesProvider.byIndex(i),
        ),
      ),
      // floatingActionButton: FloatingActionButton(
      //   onPressed: () {
      //     Navigator.of(context).pushNamed(AppRoutes.NOTES_FORM);
      //   },
      //   child: Icon(Icons.add, color: Colors.white),
      //   backgroundColor: Theme.of(context).primaryColor,
      // ),
    );
  }
}
