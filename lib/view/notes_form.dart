import 'package:calculadora_de_notas/data/user_data.dart';
import 'package:calculadora_de_notas/models/notes.dart';
import 'package:calculadora_de_notas/provider/notes_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class NotesForm extends StatefulWidget {
  @override
  _NotesFormState createState() => _NotesFormState();
}

class _NotesFormState extends State<NotesForm> {
  TextEditingController ap1Controller = TextEditingController();
  TextEditingController ap2Controller = TextEditingController();
  TextEditingController ap3Controller = TextEditingController();

  final _form = GlobalKey<FormState>();

  final Map<String, Object> _formData = {...NOTES_DATA};

  void _loadFormData(Notes notes) {
    if (notes != null) {
      _formData['id'] = notes.id;
      _formData['title'] = notes.title;
      _formData['ap1'] = notes.ap1;
      _formData['ap2'] = notes.ap2;
      _formData['ap3'] = notes.ap3;
      _formData['media'] = notes.media;
    }
  }

  void _calculate() {
    setState(() {
      double ap1 = double.parse(ap1Controller.text);
      double ap2 = double.parse(ap2Controller.text);
      double ap3 = double.parse(ap3Controller.text);

      double media = (ap1 + ap2 + ap3) / 3;

      if (media >= 0 && media < 5){
        
      } 
    });
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    final Notes notes = ModalRoute.of(context).settings.arguments;

    _loadFormData(notes);
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Adicionar notas'),
        centerTitle: true,
        backgroundColor: Theme.of(context).primaryColor,
      ),
      backgroundColor: Colors.grey[000],
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        // decoration: teste,
        child: SingleChildScrollView(
          child: Container(
            child: Column(
              children: <Widget>[
                Positioned(
                  child: Container(
                    padding: EdgeInsets.fromLTRB(32.0, 80.0, 32.0, 0.0),
                    child: Stack(
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.symmetric(vertical: 200.0),
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                            color: Theme.of(context).primaryColor,
                            borderRadius: BorderRadius.circular(32.0),
                          ),
                        ),

                        //
                        Container(
                          padding: EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 0.0),
                          // Nome da Matéria
                          child: Form(
                            key: _form,
                            child: Column(
                              children: <Widget>[
                                Container(
                                  decoration: BoxDecoration(
                                    border: Border(
                                      bottom: BorderSide(color: Colors.white),
                                    ),
                                  ),
                                  child: Padding(
                                    padding: EdgeInsets.fromLTRB(
                                        10.0, 0.0, 10.0, 0.0),
                                    child: TextFormField(
                                      style: TextStyle(color: Colors.white),
                                      initialValue: _formData['title'],
                                      decoration: InputDecoration(
                                        border: InputBorder.none,
                                        labelText: 'Nome da Matéria',
                                        labelStyle: TextStyle(
                                          color: Colors.white,
                                        ),
                                      ),
                                      validator: (value) {
                                        if (value == null ||
                                            value.trim().isEmpty) {
                                          return 'Campo Obrigatório!';
                                        }
                                        if (value.trim().length < 3) {
                                          return 'O nome deve conter no mínimo 3 letras!';
                                        }
                                        return null;
                                      },
                                      onSaved: (value) =>
                                          _formData['title'] = value,
                                    ),
                                  ),
                                ),

                                // AP1
                                Container(
                                  decoration: BoxDecoration(
                                    border: Border(
                                      bottom: BorderSide(color: Colors.white),
                                    ),
                                  ),
                                  child: Padding(
                                    padding: EdgeInsets.fromLTRB(
                                        10.0, 0.0, 10.0, 0.0),
                                    child: TextFormField(
                                      keyboardType: TextInputType.number,
                                      style: TextStyle(color: Colors.white),
                                      initialValue: _formData['ap1'],
                                      decoration: InputDecoration(
                                        border: InputBorder.none,
                                        labelText: 'Nota da AP1',
                                        labelStyle:
                                            TextStyle(color: Colors.white),
                                      ),
                                      validator: (value) {
                                        if (double.parse(value) < 0 ||
                                            double.parse(value) > 10) {
                                          return 'Número inválido';
                                        }
                                        if (double.parse(value) > 0 &&
                                            double.parse(value) < 10) {
                                          return null;
                                        } else if (value.isEmpty) {
                                          return '-.-';
                                        }
                                        return null;
                                      },
                                      onSaved: (value) =>
                                          _formData['ap1'] = value,
                                    ),
                                  ),
                                ),

                                // AP2
                                Container(
                                  decoration: BoxDecoration(
                                    border: Border(
                                      bottom: BorderSide(color: Colors.white),
                                    ),
                                  ),
                                  child: Padding(
                                    padding: EdgeInsets.fromLTRB(
                                        10.0, 0.0, 10.0, 0.0),
                                    child: TextFormField(
                                      keyboardType: TextInputType.number,
                                      style: TextStyle(color: Colors.white),
                                      initialValue: _formData['ap2'],
                                      decoration: InputDecoration(
                                        border: InputBorder.none,
                                        labelText: 'Nota da AP2',
                                        labelStyle:
                                            TextStyle(color: Colors.white),
                                      ),
                                      // validator: (value) {
                                      //   if (double.parse(value) < 0 ||
                                      //       double.parse(value) > 10) {
                                      //     return 'Número inválido';
                                      //   }
                                      //   return null;
                                      // },
                                      onSaved: (value) =>
                                          _formData['ap2'] = value,
                                    ),
                                  ),
                                ),

                                // AP3
                                Container(
                                  decoration: BoxDecoration(
                                    border: Border(
                                      bottom: BorderSide(color: Colors.white),
                                    ),
                                  ),
                                  child: Padding(
                                    padding: EdgeInsets.fromLTRB(
                                        10.0, 0.0, 10.0, 0.0),
                                    child: TextFormField(
                                      keyboardType: TextInputType.number,
                                      style: TextStyle(color: Colors.white),
                                      initialValue: _formData['ap3'],
                                      decoration: InputDecoration(
                                        border: InputBorder.none,
                                        labelText: 'Nota da AP3',
                                        labelStyle:
                                            TextStyle(color: Colors.white),
                                      ),
                                      // validator: (value) {
                                      //   if (double.parse(value) < 0 ||
                                      //       double.parse(value) > 10) {
                                      //     return 'Número inválido';
                                      //   }
                                      //   return null;
                                      // },
                                      onSaved: (value) =>
                                          _formData['ap3'] = value,
                                    ),
                                  ),
                                ),

                                // Botão Salvar
                                Container(
                                  width: MediaQuery.of(context).size.width,
                                  padding: EdgeInsets.fromLTRB(
                                      20.0, 40.0, 20.0, 40.0),
                                  child: RaisedButton(
                                    onPressed: () {
                                      final isValid =
                                          _form.currentState.validate();
                                      if (isValid) {
                                        _form.currentState.save();
                                        Provider.of<NotesProvider>(context,
                                                listen: false)
                                            .put(
                                          Notes(
                                            id: _formData['id'],
                                            title: _formData['title']
                                                .toString()
                                                .trim()
                                                .toUpperCase(),
                                            ap1: _formData['ap1']
                                                .toString()
                                                .trim(),
                                            ap2: _formData['ap2']
                                                .toString()
                                                .trim(),
                                            ap3: _formData['ap3']
                                                .toString()
                                                .trim(),
                                            media: _formData['media']
                                                .toString()
                                                .trim(),
                                          ),
                                        );
                                        Navigator.of(context).pop();
                                        _calculate();
                                      }
                                    },
                                    elevation: 7.0,
                                    color: Colors.white,
                                    splashColor: Theme.of(context).primaryColor,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(32.0),
                                    ),
                                    child: Text(
                                      'Salvar',
                                      style: TextStyle(
                                        color: Theme.of(context).primaryColor,
                                        letterSpacing: 1.5,
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
