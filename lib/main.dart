import 'package:calculadora_de_notas/models/notes.dart';
import 'package:calculadora_de_notas/provider/notes_provider.dart';
import 'package:calculadora_de_notas/routes/app_routes.dart';
import 'package:calculadora_de_notas/view/notes_form.dart';
import 'package:calculadora_de_notas/view/notes_list.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(CalculadoraDeNotas());
}

class CalculadoraDeNotas extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (ctx) => NotesProvider(),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          // primaryColor: Colors.red,
          primaryColor: Color(0xff005492),
        ),
        routes: {
          AppRoutes.HOME: (_) => NotesList(_notes),
          AppRoutes.NOTES_FORM: (_) => NotesForm(),
        },
        
      ),
    );
  }
final List<Notes> _notes = [];
}


class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
